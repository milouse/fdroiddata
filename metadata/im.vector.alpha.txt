Categories:Internet,Phone & SMS
License:Apache2
Web Site:https://vector.im
Source Code:https://github.com/vector-im/vector-android
Issue Tracker:https://github.com/vector-im/vector-android/issues

Auto Name:Vector
Summary:Open team chat
Description:
Vector gathers all your conversations and app integrations into one single app.

Built around group chatrooms, Vector lets you share messages, images, videos and
files - interact with your tools and access all your different communities under
one roof. One single identity and place for all your teams: no need to switch
accounts, work and chat with people from different organisations in public or
private rooms: from professional projects to school trips, Vector will become
the center of all your discussions!

Features include:

* Instantly share messages, images, videos and files of any kind within groups of any size
* Voice and video 1-1 and conference calling via WebRTC
* See who's reading your messages with read receipts
* Communicate with users anywhere in the Matrix.org ecosystem - not just Vector users!
* Discover and invite users by email address
* Participate in guest-accessible public rooms
* Highly scalable - supports hundreds of rooms and thousands of users
* Fully synchronised message history across multiple devices and browsers
* Finely configurable notification settings, synchronised over all devices
* Infinite searchable chat history
* Permalinks to messages
* Full message search
* Excellent support for all Android device sizes and orientations

Note that the F-Droid release does not use GCM for notifications - instead it
will keep syncing in the background. If you find that the ongoing background
sync is using too much battery, you can add a delay or change the timeout of the
sync or even disable background sync completely, in the settings page.

For developers:

* Vector is a Matrix client - built on the Matrix.org open standard and ecosystem, providing interoperability with all other Matrix compatible apps, servers and integrations
* Entirely open sourced under the permissive Apache License - get the code from [https://github.com/vector-im/vector-android]. Pull requests welcome!
* Trivially extensible via the open Matrix Client-Server API ([https://matrix.org/docs/spec])
* Run your own server! You can use the default matrix.org server or run your own Matrix home server (e.g. [https://matrix.org/docs/projects/server/synapse.html])

Coming soon:

* Add your own integrations, bridges and bots!
* Email notifications of missed messages and invites
* End-to-end encryption using Olm ([https://matrix.org/git/olm])
* Screen sharing
* Login as multiple users at the same time

Discover truly efficient and open collaboration with Vector!
.

Repo Type:git
Repo:https://github.com/vector-im/vector-android

Build:0.3.2,14
    commit=5e1607ed70e3f0219cb837b7706ead4b97033928
    subdir=vector
    gradle=appfdroid
    srclibs=matrixsdk@a54a155f9e8b250c0b077979c0d5a24ac1b8ced8
    prebuild=cp -fR $$matrixsdk$$/matrix-sdk ../ && \
        sed -i -e '/projectDir/d' ../settings.gradle && \
        sed -i -e '/play-services/d' build.gradle

Build:0.3.4,304
    commit=290464bb0fe7db1e8393541160f149d7ee04b417
    subdir=vector
    gradle=appfdroid
    srclibs=matrixsdk@80a629ab18d525beabeebec6ab9065b776e94455
    prebuild=cp -fR $$matrixsdk$$/matrix-sdk ../ && \
        sed -i -e '/projectDir/d' ../settings.gradle && \
        sed -i -e '/play-services/d' build.gradle

Build:0.4.0,400
    commit=c7724d8c148e454c338ed33c81b85d55b0985c87
    subdir=vector
    gradle=appfdroid
    srclibs=matrixsdk@2ed84ed78fa2854ad00041e591ea0f040c9d4b7b
    prebuild=cp -fR $$matrixsdk$$/matrix-sdk ../ && \
        sed -i -e '/projectDir/d' ../settings.gradle && \
        sed -i -e '/play-services/d' build.gradle

Auto Update Mode:None
Update Check Mode:None
Current Version:0.4.0
Current Version Code:400
