Categories:System
License:Apache2
Web Site:
Source Code:https://github.com/SimpleMobileTools/Simple-Calendar
Issue Tracker:https://github.com/SimpleMobileTools/Simple-Calendar/issues
Changelog:https://github.com/SimpleMobileTools/Simple-Calendar/blob/HEAD/CHANGELOG.md

Auto Name:Calendar
Summary:A calendar with customizable widget and events
Description:
A monthly calendar with the current day highlighted, without any other calendar
integration, with Monday as the first day. You can easily check other months or
years by pressing the month label in the app. You can also create events.

Contains a resizable 4x4 widget where you can customize the color of the text,
as well as the alpha and the color of the background. To open the app from the
widget press the month label.
.

Repo Type:git
Repo:https://github.com/SimpleMobileTools/Simple-Calendar

Build:1.6,7
    commit=7c920510354f9ce7a3e6ddc13b39960b50d09811
    subdir=app
    gradle=yes

Build:1.8,8
    commit=1.8
    subdir=app
    gradle=yes

Build:1.9,9
    commit=1.9
    subdir=app
    gradle=yes

Build:1.11,11
    commit=1.11
    subdir=app
    gradle=yes

Build:1.12,12
    commit=1.12
    subdir=app
    gradle=yes

Build:1.14,14
    commit=1.14
    subdir=app
    gradle=yes

Build:1.15,15
    commit=1.15
    subdir=app
    gradle=yes

Build:1.16,16
    commit=1.16
    subdir=app
    gradle=yes

Build:1.17,17
    commit=1.17
    subdir=app
    gradle=yes

Build:1.18,18
    commit=1.18
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.18
Current Version Code:18
